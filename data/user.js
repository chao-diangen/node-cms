const mysqlConnection = require('./mysqlconnect')
const onSelectOnlyUser = function (params) {
    return new Promise(function (resolve, reject) {
        const name = params.name
        const password = params.password
        let selectSql = `SELECT * FROM login WHERE user_name = '${name}' AND user_password = '${password}'`;
        //更新
        mysqlConnection.query(selectSql, function (err, result) {
            if (err) {
                console.log('[INSERT ERROR] - ', err.message);
                reject(err)
                return;
            }
            resolve(result)
        });
    })
}
const onSelectAllUser = function (params) {
    return new Promise(function (resolve, reject) {
        const page_num = params.pageNumber //当前的num
        const page_size = params.pageSize //当前页的数量
        const newParams = [(parseInt(page_num) - 1) * parseInt(page_size), parseInt(page_size)]
        let selectSql = `SELECT * FROM user  LIMIT ?,?`
        // 获取总页数
        mysqlConnection.query(selectSql, newParams, function (err, result) {
            if (err) {
                console.log('[SELECT ERROR] - ', err.message);
                reject(err)
                return;
            } else {
                let sqlTotal = 'select count(*) as total from user' //as更换名称
                mysqlConnection.query(sqlTotal, function (error, among) {
                    if (error) {
                        console.log(error);
                    } else {
                        let total = among[0]['total'] //查询表中的数量
                        resolve({
                            code: 0,
                            status: 200,
                            message: "success",
                            data: result,
                            page: {
                                page_num: page_num,
                                page_size: page_size,
                                total: total
                            }
                        })
                    }
                })
            }

        })
    })
}
const onSelectOnlyInfo = function (params) {
    return new Promise(function (resolve, reject) {
        const id = params.id
        let selectSql = `SELECT * FROM user WHERE for_user_id = '${id}'`;
        //查询数据
        mysqlConnection.query(selectSql, function (err, result) {
            if (err) {
                console.log('[INSERT ERROR] - ', err.message);
                reject(err)
                return;
            }
            resolve(result)
        });
    })
}

const onSelectOnlyMenu = function (params) {
    return new Promise(function (resolve, reject) {
        const id = params.id
        let selectSql = `SELECT * FROM manage WHERE role_type = '${id}'`;
        //查询数据
        mysqlConnection.query(selectSql, function (err, result) {
            if (err) {
                console.log('[INSERT ERROR] - ', err.message);
                reject(err)
                return;
            }
            resolve(result)
        });
    })
}


module.exports = {
    onSelectOnlyUser,
    onSelectAllUser,
    onSelectOnlyInfo,
    onSelectOnlyMenu
}
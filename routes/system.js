var express = require('express');
var router = express.Router();
const systemDao = require('../data/system.js')
router.post('/menu/list', async function (req, res, next) {
    let result = await systemDao.getMenuList(req.body)
    res.send(result)
    return
});

module.exports = router;
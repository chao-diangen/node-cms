const jwt = require('jsonwebtoken');
const token = {
    //  生成
    encrypt: function (data) { //data加密数据 ，time过期时间  60 * 30  （30分）

        let token = jwt.sign({
            data: data
        }, 'Fizz', {
            expiresIn: 60 * 60 * 24
        })
        return token;
    },
    // 解析
    decrypt: function (token) {
        try {
            let data = jwt.verify(token, 'Fizz', function (err, decoded) {
                if (err) {
                    return res.status(400).send(next(createError(400, '无效的token')));
                } else {
                    // 如果验证通过，在req中写入解密结果
                    console.log(decoded) //{ userId: 31, iat: 1574244315, exp: 1574330715 }
                    req.decoded = decoded;
                    next();
                }
            });
            console.log('data', data)
            return {
                token: true
            };
        } catch (e) {
            return {
                token: false,
                data: e
            }
        }
    }
}
module.exports = token;